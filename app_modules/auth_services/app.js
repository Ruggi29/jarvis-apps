var fs = require("fs");
var _ = require("underscore");
var auth_data_path=process.env.HOME+"/Dropbox/auth_data/authentications.json";

var init=function(){
	startHttpServiceWithAuthData();
}

var readAuthFile=function(callback){
	fs.readFile(auth_data_path, 'utf8', function (err,data) {
	  if (err) {
	    return console.log(err);
	  }
		callback(JSON.parse(data));
	});
}

var startHttpServiceWithAuthData=function(){
	var http = require('http');
	http.createServer(function (req, res) {
		  readRequestBody(req, function(requestBody){
			if(!validateRequestBody(requestBody)){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('invalid format');
				return;
			}
			readAuthFile(function(auth_data){
				if(!validateAuthentication(auth_data, requestBody.app_name, requestBody.app_secret)){
					res.writeHead(200, {'Content-Type': 'text/plain'});
					res.end('invalid crediential');
					return;
				}
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('success');
			})
		  })
	}).listen(HTTP_PORT, HTTP_ADDRESS);
	console.log("Server running at "+HTTP_ADDRESS+" and port "+HTTP_PORT);
}

exports.authenticationHttpHandler=function(req, res){
	var requestBody=req.body;
	if(!validateRequestBody(requestBody)){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('invalid format');
		return;
	}
	readAuthFile(function(auth_data){
		if(!validateAuthentication(auth_data, requestBody.app_name, requestBody.app_secret)){
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('invalid crediential');
			return;
		}
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('success');
	})
};

var validateRequestBody=function(body){
	return body.hasOwnProperty("app_name")&&body.hasOwnProperty("app_secret");
}

var validateAuthentication=function(auth_data, app_name, app_secret){
	var result=_.find(auth_data, function(auth){
				return auth.app_name==app_name&&auth.app_secret==app_secret;
			})
	return result!=null;
}